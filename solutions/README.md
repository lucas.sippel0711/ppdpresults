# How to Read Solution Folders and Files

## Solution Folders

Folder names are formatted as `solutions_{algorithm}_{formulation}` where algorithm is either `exact`, `exacthf`, `cg2`, `cg3`, `cg4`, `cg5`, or `cg6`, and `formulations` is either `rf2` or `spp`.  The two different formulations correspond to the two formulations from the paper.  As for `algorithm`, `exact` and `exacthf` correspond to solving the formulation with the entire set of fragments or routes, with large and small fixed vehicle cost respectively, whereas `cg{number}` corresponds to using extension restriction with $\kappa_2 = $ `number`.  

Solutions for formulation SPP that use extension restriction are only for the large vehicle fixed cost case.  Also, solutions for formulation RF2 that use extension restriction are only for the small vehicle fixed cost case.

## Reading a Solution File
Each solution file has one line containing firstly the best objective value found, secondly, whether the instance was solved to optimality, and finally the best solution found.