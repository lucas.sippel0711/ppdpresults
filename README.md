# Reading Instances

## Folder
Test instances are found in `test_instances`.

## How to Read Files
Each instance can be read into python via
```Python
with open(filename,'r') as file_:
        contents = eval(file_.read())
    contents = {k: v for k,v in contents}
    t = eval(contents['t'])
    tw = eval(contents['tw'])
    dist = eval(contents['dist'])
    tp = eval(contents['tp'])
    w = eval(contents['weight'])
    Q = eval(contents['Q'])
    alpha = eval(contents['alpha'])
    beta = eval(contents['beta'])
    F = eval(contents['F'])
```
where ```filename``` is the name of the instance.  Variable ```t``` is a list of lists such that ```t[i][j]``` gives the direct travel time from request i to j.  Variable ```tw``` is a list of lists such that ```tw[i][j]``` gives the travel time given the minimum number of breaks and rests are inserted from request i to j.  Variable ```dist``` is a list of lists such that ```dist[i][j]``` gives the travel distance from request i to j.  Variable ```tp``` is a list of lists such that ```t[i]``` gives the list of times when service can start at request i.  Each time point is a multiple of 30 minutes.  Variable ```w``` is a list such that ```w[i]``` gives the weight of request i.  Variable ```Q``` gives the capacity of each vehicle.  Variable `alpha` gives the mileage cost per unit distance.  Variable `beta` gives the waiting cost per half hour.  Finally variable `F` gives the fixed cost of using a vehicle.
